all:
	pdflatex ixpe_example

clean:
	rm -f *~ *.aux *.log *.out *.snm *.nav *.toc

cleanall: clean
	rm -r ixpe_example.pdf
